package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	
	int capacity = 20;
	int currentCapacity;
	
	public static void main(String[] args) {
		
	
	}
	ArrayList<FridgeItem> fridgeItems = new ArrayList<>();
	
	
	@Override
	public int nItemsInFridge() {
		return fridgeItems.size();
	}

	@Override
	public int totalSize() {
		return capacity; 	
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		
		if (!(fridgeItems.size() == 20)) {
			fridgeItems.add(item);
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (fridgeItems.contains(item)) {
			fridgeItems.remove(item);
	}
		else throw new NoSuchElementException();
	
	}

	@Override
	public void emptyFridge() {
		fridgeItems.removeAll(fridgeItems);
			}
		
		
	
	@Override
	public ArrayList<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredItems = new ArrayList<>();
		for(FridgeItem item : fridgeItems) {
			if (!item.hasExpired()) {
				expiredItems.add(item);
			}
				
		}
		fridgeItems.removeAll(expiredItems);
		return fridgeItems;
		}

}